from faker import Faker
import re
from loader import *
from datetime import datetime
from PIL import Image

fonts = glob.glob('fonts/*')
from PIL import Image, ImageDraw, ImageFont
from itertools import dropwhile
import random

ldrop = lambda fn, x: list(dropwhile(fn, x))
lrev = lambda x: list(reversed(x))
gBlur = lambda x, k=5: cv2.GaussianBlur(x, (k,k), k)
chop = lambda x, fn=lambda x: x==0: np.array(lrev(ldrop(fn, lrev(ldrop(fn, x)))))
clean_img = lambda img: 1 - chop(chop(1-img, fn=lambda x: x.sum()==0).T, fn=lambda x: x.sum()==0).T
clean_img = lambda img: 1 - chop(chop(1-img, fn=lambda x: x.sum()==0).T, fn=lambda x: x.sum()==0).T

def make_image(text, sz, font=None):
    img = Image.new('RGB', sz, color = (255, 255, 255))
    d = ImageDraw.Draw(img)
    d.text((10,10), text, font=font, fill=(0,0,0))
    img = np.array(img.convert(mode='1')).astype(np.uint8)
    return clean_img(img)

def make_image_char(text, sz, font=None, fonts=None, f_s=0, si_s=0, l_s =0, sp_s=0):
    img = Image.new('L', sz, color = 255)
    d = ImageDraw.Draw(img)
    if font is None:
        _font = np.random.choice(fonts)
#         _font = 'hw-fonts-3k/Briasco Rustic.ttf'
        font = ImageFont.truetype(_font, 100)
    elif isinstance(font, str):
        _font = font
        font = ImageFont.truetype(_font, 50)
    for i, char in enumerate(list(text)):
        if np.random.rand() < f_s:
            font = np.random.choice(fonts)
            font = ImageFont.truetype(font, 20)
        if np.random.rand() < si_s:
            k = np.random.randint(75, 125)
            font = ImageFont.truetype(_font, k)
        if np.random.rand() < l_s:
            h = np.random.randint(-20, 20)
        else:
            h = 0
        if i ==0:
            start = 0

        else:
            _img = np.array(img).astype(np.uint8)
            min_list = list(np.min(_img, axis = 0))
            ind = min_list[::-1].index(0)
            start = len(min_list)-ind
            start = start - randint(10)
            if text[i-1] == ' ':
                start += 50
        if np.random.rand() < sp_s:
            w = np.random.randint(0,10)
        else:
            w = 0
        d.text((start+w, 10+h), char, font=font, fill=0)

    img = np.array(img.convert(mode='1')).astype(np.uint8)
    return clean_img(img), _font

def _make_image(string, font):
    img = rand_font_img(string, (3000,1000), font=font)
    h = len(img)
    # h_ = np.random.randint(20, 100)
    # f = h_/h
    # img = cv2.resize(img, dsize=None, fx=f, fy=f, interpolation = cv2.INTER_AREA)
    return img


def rand_font_img(text, sz, font=None):
    if font is None:
        font = np.random.choice(fonts)
    return make_image(text, sz, ImageFont.truetype(font, 100))

def _make_image_char(string, font, fonts, font_shuffle=0, size_shuffle=0, line_shuffle =0, space_shuffle=0):
    img, font = make_image_char(string, (3000,1000), font=font, fonts=fonts, f_s=font_shuffle, si_s=size_shuffle, l_s =line_shuffle, sp_s=space_shuffle)
    h = len(img)
    # h_ = np.random.randint(500, 1000)
    # h_ = np.random.randint(20, 100)
    # f = h_/h
    # img = cv2.resize(img, dsize=None, fx=f, fy=f, interpolation = cv2.INTER_AREA)
    return img, font


class MakeText:
    def __init__(self, fake=Faker()):
        super().__init__()
        self.fake = fake

    def __call__(self, _type=None, inject=None):
        _type = np.random.choice(self.types) if _type is None else _type
        self.inject = list(inject) if inject is not None else None
        # if _type not in self.types: raise NotImplementedError
        if _type == 'words': return self.postprocess(self.words())
        if _type == 'currency': return self.postprocess(self.currency())
        if _type == 'date': return self.postprocess(self.date())
        return self.postprocess(getattr(self.fake, _type)())

    def words(self):
        words = [self.fake.word() for _ in range(np.random.randint(2, 4))]
        words = [w.capitalize() if self.rng()>0.5 else w for w in words]
        return ' '.join(words)

    def currency(self):
        spaces = ' '*np.random.randint(3)
        decimals = np.random.randint(1,3)
        num = np.random.randint(100000)/100
        if self.rng() > 0.5: return '${}{}'.format(spaces, num)
        else: return '${}{}'.format(spaces, int(num))

    def iso8601(self):
        text = list(self.fake.iso8601())
        np.random.shuffle(text)
        n = 6 + randint(9)
        text = ''.join(text)
        return text[:n]

    def password(self):
        text = list(self.fake.password() + self.fake.password())
        np.random.shuffle(text)
        n = 6 + randint(9)
        text = ''.join(text)
        return text[:n]

    def date(self):
        txt = self.fake.date()
        formats = ["%Y-%m-%d",'%Y-%d-%m','%d-%m-%Y','%m-%d-%Y','%Y-%b-%d','%m ,%d ,%Y','%Y-%d-%b',
                   "%b%d,%Y",
                   '%b-%d',"%d-%b","%b %d","%d %b","%b%d","%d%b",'%b,%d',"%d,%b","%Y/%m/%d",'%Y/%d/%m',
                   '%d/%m/%Y','%m-%d-%Y','%Y - %b - %d','%m  , %d  , %Y','%Y - %d - %b',
                   "%b %d , %Y",'%b -%d',"%d - %b","%b  %d","%d  %b","%b%d","%d%b",
                   '%B -%d',"%d - %B","%B  %d","%d  %B","%B%d","%d%B"]

        j = np.random.randint(len(formats))
        txt = datetime.strptime(txt,'%Y-%m-%d').strftime(formats[j])
        if self.rng() > 0.5: return txt.replace('-', '/')
        else               : return txt

    def rand(self):
        return self(np.random.choice(self.types))

    def rng(self):
        return np.random.rand()

    def postprocess(self, text):
        if self.inject is None: return text
        else:
            for _ in range(np.random.randint(2, 5)):
                ix = np.random.randint(len(text))
                text = text[:ix] + np.random.choice(self.inject) + text[ix:]
        textlen = np.random.randint(6,10)
        if len(text)-textlen <= 0: return text
        else:
            s = np.random.randint(len(text)-textlen)
            return text[s:s+textlen]
