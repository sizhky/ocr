from loader import *
import torch
from torch.utils.data import DataLoader, Dataset
import imgaug as ia
from imgaug import augmenters as iaa

class ImgAugTransform:
    def __init__(self):
        self.aug = iaa.Sequential([
            iaa.Pad((5, 20), pad_cval=255),
            iaa.Sometimes(0.25, iaa.GaussianBlur(sigma=(0, 3.0))),
            iaa.Affine(rotate=(-5, 5), mode='constant', cval=255),
            iaa.Sometimes(0.5, iaa.Dropout(p=(0.01, 0.07)))
        ])

    def __call__(self, img):
        img = np.array(img)
        return self.aug.augment_image(img)

class ocr_dataset(Dataset):
    def __init__(self, folder, validation=False):
        super().__init__()
        self.is_validation = validation
        self.transform = ImgAugTransform()
        self.files = Glob(folder)

    def __len__(self): return len(self.files)

    def __getitem__(self, ix):
        f = self.files[ix]
        im = read(f)
        if not self.is_validation: im = self.transform(im)
        f = stem(f)
        label, font = f.split('@')[:2]
        return self.preprocess(im), label, font

    def preprocess(self, img, random_placement=False, noise=False):
        img = img.mean(-1).astype(np.uint8) if len(img.shape) == 3 else img
        target = np.ones((40, 400))*255
        new_x = 40/img.shape[0]
        new_y = 400/img.shape[1]
        min_xy = min(new_x, new_y)
        new_x = int(img.shape[0]*min_xy)
        new_y = int(img.shape[1]*min_xy)
        if not all([new_x,new_y]): return np.zeros((40,400))
        img2 = cv2.resize(img, (new_y,new_x))
        target[:new_x,:new_y] = img2
        return torch.tensor(1 - (target)/255).type(torch.float32)
