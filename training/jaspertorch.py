"""Modified version of "Jasper: An End-to-End Convolutional Neural Acoustic Model".
Residual connections are implemented differently than the original.

See:
https://github.com/NVIDIA/OpenSeq2Seq/blob/master/example_configs/speech2text/jasper10x5_LibriSpeech_nvgrad.py
"""
__author__ = 'yyr'

import numpy as np, cv2, editdistance
import torch
import torch.nn as nn
import torch.nn.functional as F

class TinyJasper(nn.Module):
    charList = charList = [' ', '!', '"', '#', '$', '%', '&', "'", '(', ')',
                            '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6',
                            '7', '8', '9', ':', ';', '=', '?', '@', 'A', 'B', 'C', 'D',
                            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', ']',
                            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    int2char = {ix:ch for ix, ch in enumerate(charList)}
    int2char.update({len(charList): '`'})
    timesteps = ts = 100

    def __init__(self, model_weights_path=None):
        super().__init__()
        self.arch()
        self.iters = 0

    def arch(self):
        self.first_layer = C(40, 128, 3, stride=2, dropout_rate=0.2)
        self.second_layer = C(128, 128, 3, stride=2, dropout_rate=0.2)

        self.B1 = nn.Sequential(
            C(128, 256, 3, dropout_rate=0.2),
            C(256, 256, 3, dropout_rate=0.2),
            C(256, 256, 3, dropout_rate=0.2),
        )
        self.r1 = nn.Conv1d(128, 256, 1)

        self.B2 = nn.Sequential(
            C(256, 384, 3, dropout_rate=0.2),
            C(384, 384, 3, dropout_rate=0.2),
            C(384, 384, 3, dropout_rate=0.2),
        )
        self.r2 = nn.Conv1d(256, 384, 1)

        self.B3 = nn.Sequential(
            C(384, 512, 5, dropout_rate=0.2),
            C(512, 512, 5, dropout_rate=0.2),
            C(512, 512, 5, dropout_rate=0.2),
        )
        self.r3 = nn.Conv1d(384, 512, 1)

        self.B4 = nn.Sequential(
            C(512, 640, 7, dropout_rate=0.3),
            # C(640, 640, 21, dropout_rate=0.3),
            # C(640, 640, 21, dropout_rate=0.3),
        )
        self.B5 = nn.Sequential(
            C(640, 768, 9, dropout_rate=0.3),
            # C(768, 768, 25, dropout_rate=0.3),
            # C(768, 768, 25, dropout_rate=0.3),
        )
        self.r4_5 = nn.Conv1d(512, 768, 1)

        self.last_layer = nn.Sequential(
            # C(768, 896, 29, dropout_rate=0.4, dilation=2),
            C(768, 896, 11, dropout_rate=0.4),
            C(896, 1024, 11, dropout_rate=0.4),
            C(1024, len(self.charList)+1, 1)
        )

    def forward(self, x):
        y = self.first_layer(x)
        y = self.second_layer(y)
        y = self.B1(y) + self.r1(y)
        y = self.B2(y) + self.r2(y)
        y = self.B3(y) + self.r3(y)
        y = self.B5(self.B4(y)) + self.r4_5(y)

        y = self.last_layer(y)
        return nn.functional.log_softmax(y, dim=1)

    def preprocess(self, img, random_placement=False, noise=False):
        img = img.mean(-1).astype(np.uint8) if len(img.shape) == 3 else img
        target = np.ones((40, 400))*255
        new_x = 40/img.shape[0]
        new_y = 400/img.shape[1]
        min_xy = min(new_x, new_y)
        new_x = int(img.shape[0]*min_xy)
        new_y = int(img.shape[1]*min_xy)
        if not all([new_x,new_y]): return np.zeros((40,400))
        img2 = cv2.resize(img, (new_y,new_x))
        target[:new_x,:new_y] = img2
        return torch.tensor(1 - (target)/255).type(torch.float32)

    def str2vec(self, string):
        string = ''.join([s for s in string if s in self.charList])
        val = list(map(lambda x: self.charList.index(x), string))[:self.ts//2]
        while len(val) < self.ts:
            val.append(len(self.charList))
        return torch.tensor(val).long()

    def decoder_chars(self, pred):
        pred = pred.cpu().detach().numpy().T
        decoded = ""
        last = ""
        for i in range(len(pred)):
            k = np.argmax(pred[i])
            if k < len(self.charList) and self.charList[k] != last:
                last = self.charList[k]
                decoded = decoded + last
            elif k < len(self.charList) and self.charList[k] == last:
                last == self.charList[k]
            else:
                last = ""
        return decoded.replace("  "," ")

    def evaluate(self, ims, labels, lower=False):
        self.training = False
        # ims = np.array([self.preprocess(im) for im in ims])
        preds = self(ims)
        preds = [self.decoder_chars(pred) for pred in preds]
        if lower:
            preds = [p.lower() for p in preds]
            labels = [l.lower() for l in labels]
        return {'char-error-rate': self.cer(preds, labels),
                'word-error-rate': self.wer(preds, labels),
                'char-accuracy'  : 1 - self.cer(preds, labels),
                'word-accuracy'  : 1 - self.wer(preds, labels)}

    def wer(self, preds, labels):
        assert isinstance(preds, (list, tuple))
        assert isinstance(labels, (list, tuple))
        assert len(preds) == len(labels)
        c = 0
        for p, l in zip(preds, labels):
            c += p.lower().strip() != l.lower().strip()
        return round(c/len(preds), 4)

    def cer(self, preds, labels):
        assert isinstance(preds, (list, tuple))
        assert isinstance(labels, (list, tuple))
        assert len(preds) == len(labels)
        c, d = [], []
        for p, l in zip(preds, labels):
            # c.append(editdistance.eval(p, l))
            # d.append(len(l))
            c.append(editdistance.eval(p, l) / len(l))
        # return np.sum(c)/np.sum(d)
        return round(np.mean(c), 4)

"""Common layers."""
class C(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, dilation=1, activation='relu', padding=None, dropout_rate=0.0):
        """1D Convolution with the batch normalization and RELU."""
        super(C, self).__init__()
        self.activation = activation
        self.dropout_rate = dropout_rate

        assert 1 <= stride <= 2
        if dilation > 1:
            assert stride == 1
            padding = (kernel_size - 1) * dilation // 2
        else:
            padding = (kernel_size - stride + 1) // 2

        self.conv = nn.Conv1d(in_channels, out_channels, kernel_size, stride=stride, padding=padding, dilation=dilation)
        nn.init.xavier_uniform_(self.conv.weight, nn.init.calculate_gain('relu'))

        self.bn = nn.BatchNorm1d(out_channels)

    def forward(self, x):
        y = self.conv(x)
        y = self.bn(y)

        if self.activation == 'relu':
            y = F.relu(y, inplace=True)
            # OpenSeq2Seq uses max clamping instead of gradient clipping
            # y = torch.clamp(y, min=0.0, max=20.0)  # like RELU but clamp at 20.0

        if self.dropout_rate > 0:
            y = F.dropout(y, p=self.dropout_rate, training=self.training, inplace=False)
        return y

class CTCLossNM:
    """ CTC loss"""
    def __init__(self, **kwargs):
        self._blank = kwargs['num_classes'] - 1
        self._criterion = nn.CTCLoss(blank=self._blank, reduction='none', zero_infinity=False)

    def __call__(self, log_probs, targets, input_length, target_length):
        input_length = input_length.long()
        target_length = target_length.long()
        targets = targets.long()
        log_probs = log_probs.permute(2, 0, 1)
        # print('log_probs: {}\ntargets: {}\ninput_len: {}\ntarget_len: {}'.format(log_probs.shape, targets.shape, input_length.shape, target_length.shape))
        loss = self._criterion(log_probs, targets, input_length,
                                                     target_length)
        # note that this is different from reduction = 'mean'
        # because we are not dividing by target lengths
        return torch.mean(loss)
