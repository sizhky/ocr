# +
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.layers import *
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
import tensorflow.keras as keras
import cv2, tqdm, numpy as np
from itertools import groupby
from loguru import logger
import editdistance

def ctc_fl(args):
    y_pred, labels, input_length, label_length = args
    ctc_loss = K.ctc_batch_cost(labels, y_pred, input_length, label_length)
    p = tf.exp(-ctc_loss)
    gamma = 0.5 #0#0.5
    alpha=0.25 #1#0.25
    return alpha*(K.pow((1-p),gamma))*ctc_loss

def ctc_loss(args):
    y_pred, labels, input_length, label_length = args
    return K.ctc_batch_cost(labels, y_pred, input_length, label_length)

BATCH_NORM_EPSILON = 1e-5
BATCH_NORM_DECAY = 0.997
def batch_norm(inputs):
    return BatchNormalization(momentum= BATCH_NORM_DECAY, epsilon = BATCH_NORM_EPSILON)(inputs)

def jasper_unit(data, filters, kernel, stride, dropouts):

    old = batch_norm(data)

    #First Block
    data = Conv1D(filters = filters, kernel_size = (kernel), strides = (stride),
                  padding = "same")(data)
    data = batch_norm(data)
    data = Activation('relu')(data)
    data = Dropout(dropouts)(data)

    #Second Block
    data = Conv1D(filters = filters, kernel_size = (kernel), strides = (stride),
                  padding = "same")(data)
    data = batch_norm(data)
    data = Activation('relu')(data)
    data = Dropout(dropouts)(data)

    #Third Block
    data = Conv1D(filters = filters, kernel_size = (kernel), strides = (stride),
                  padding = "same")(data)
    data = batch_norm(data)

    final = Concatenate()([old,data])

    data = Activation('relu')(data)
    data = Dropout(dropouts)(data)

    return data

def jasper_arch(nc, input_shape=(40,400)):
    input_data = Input(name='the_input', shape = input_shape, dtype='float32')
    final = Permute((2, 1))(input_data)
    final

    data = Conv1D(filters = 128, kernel_size = (3), strides = (2), padding = "same")(final)
    data = batch_norm(data)
    data = Activation('relu')(data)
    data = Dropout(0.2)(data)

    data = Conv1D(filters = 128, kernel_size = (3), strides = (2), padding = "same")(data)
    data = batch_norm(data)
    data = Activation('relu')(data)
    data = Dropout(0.2)(data)

    # 5 * 3 Jasper Blocks
    data = jasper_unit(data,256, 3, 1, 0.2)
    data = jasper_unit(data,384, 3, 1, 0.2)
    data = jasper_unit(data,512, 4, 1, 0.3)
    data = jasper_unit(data,640, 5, 1, 0.3)
    data = jasper_unit(data,768, 6, 1, 0.3)

    data = Conv1D(filters = 896, kernel_size = (7), strides = (1), padding = "same", dilation_rate = 2)(data)
    data = batch_norm(data)
    data = Activation('relu')(data)
    data = Dropout(0.4)(data)

    data = Conv1D(filters = 1024, kernel_size = (1), strides = (1), padding = "same")(data)
    data = batch_norm(data)
    data = Activation('relu')(data)
    data = Dropout(0.4)(data)

    data = Conv1D(filters = nc+1, kernel_size = (1), strides = (1), padding = "same")(data)
    y_pred = Activation('softmax',name="pred_layer")(data)

    Optimizer = Adam(lr = 0.001)

    labels = Input(name = 'the_labels', shape=[100], dtype='float32')
    input_length = Input(name='input_length', shape=[1],dtype='int64')
    label_length = Input(name='label_length',shape=[1],dtype='int64')
    output = Lambda(ctc_fl, output_shape=(1,),name='ctc')([y_pred, labels, input_length, label_length])

    model = Model(inputs = [input_data, labels, input_length, label_length], outputs= output)
    model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer = Optimizer)
    return model


class JasperOCR:
    charList = charList = [' ', '!', '"', '#', '$', '%', '&', "'", '(', ')',
                            '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6',
                            '7', '8', '9', ':', ';', '=', '?', '@', 'A', 'B', 'C', 'D',
                            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', ']',
                            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    int2char = {ix:ch for ix, ch in enumerate(charList)}
    int2char.update({len(charList): '`'})
    timesteps = ts = 100

    def __init__(self, model_weights_path=None, model_path=None, charlist=None, input_shape=(40,400), load_language_model=False):
        if charlist is not None:
            self.charList = charlist
            self.int2char = {ix:ch for ix, ch in enumerate(self.charList)}
            self.int2char.update({len(self.charList): '`'})
        if model_path:
            self.model = keras.models.load_model(model_path, custom_objects={'<lambda>': lambda x, y: y,
                                                 'tf':tf})
        else:
            self.model = jasper_arch(len(self.charList), input_shape=input_shape)

        if model_weights_path is not None:
            self.model.load_weights(model_weights_path)
        self.get_prediction_model()
        if load_language_model:
            from spellcorrect import order_preds_beam
            self.order_preds_beam = order_preds_beam

    def get_prediction_model(self):
        # self.ocr = keras.models.Model(self.model.get_layer('the_input').input, self.model.get_layer('pred_layer').output)
        self.ocr = keras.models.Model(self.model.get_layer('the_input').input, self.model.get_layer('pred_layer').output)
        logger.info('Predicition model is at self.ocr')

    def str2vec(self, string):
        string = ''.join([s for s in string if s in self.charList])
        val = list(map(lambda x: self.charList.index(x), string))[:self.ts//2]
        while len(val) < self.ts:
            val.append(len(self.charList))
        return val

    def datafy(self, ims, labels):
        'convert images and labels to a format usable for training ocr'
        assert isinstance(ims, (list, tuple))
        assert isinstance(labels, (list, tuple))
        assert len(ims) == len(labels)

        ims = np.array([self.preprocess(im) for im in ims])# [...,None]
        label_lengths = np.array([len(label) for label in labels])
        labels = np.array([self.str2vec(label) for label in labels])
        input_lengths = np.array([self.ts]*len(labels))
        inputs = {
            'the_input': ims,
            'the_labels': labels,
            'input_length': input_lengths,
            'label_length': label_lengths,
        }

        outputs = {'ctc': np.zeros([len(labels)])}
        return inputs, outputs

    def decoder_chars(self, pred):
        decoded = ""
        last = ""
        for i in range(len(pred)):
            k = np.argmax(pred[i])
            if k < len(self.charList) and self.charList[k] != last:
                last = self.charList[k]
                decoded = decoded + last
            elif k < len(self.charList) and self.charList[k] == last:
                last == self.charList[k]
            else:
                last = ""
        return decoded.replace("  "," ")

    def predict(self, im, vocab=None, debug=False, return_array=False, conf_agg='min'):
        if vocab is None: vocab = self.charList
        valid_ixs = [self.charList.index(i) for i in vocab] + [len(self.charList)]
        invalid_ixs = [i for i in range(len(self.charList)+1) if i not in valid_ixs]
        img = self.preprocess(im)
        pred = self.ocr.predict(img[None])[0]
        pred[:,invalid_ixs] = 0
        if return_array: return pred
        prob = list(zip(pred.argmax(-1), pred.max(-1)))
        prob = [(self.int2char[c], conf) for c, conf in prob]
        tup = [(k, max(g, key=lambda x: x[1])[1]) for k, g in groupby(prob, key=lambda x: x[0])]
        tuples = [t for t in tup if t[0]!='`']
        if debug: print([(k,max(g, key=lambda x: x[1])) for k, g in groupby(prob, key=lambda x: x[0])])
        if conf_agg == 'min':
                conf = min([p for p in tup if p != '`'], key=lambda x: x[1])[1]
        elif conf_agg == 'mean':
            conf = np.mean([p[1] for p in tup if p != '`'])
        else:
            raise NotImplementedError('Acceptable aggregate functions are `min` and `mean`')
        conf = min([p for p in tup if p != '`'], key=lambda x: x[1])[1]
        pred2 = pred
        pred = self.decoder_chars(pred)
        return pred, conf, tuples
        # return pred, conf
    def predict_batch(self, imgs, vocab=None, debug=False, return_array = False, conf_agg='min'):
        batch_size=4096
        N = len(imgs)
        n_batches = N//batch_size + 1
        def yield_batch(imgs):
            for i in range(n_batches):
                batch = imgs[i*batch_size:(i+1)*batch_size]
                yield batch
        batches = yield_batch(imgs)
        for ix, batch in tqdm.tqdm(enumerate(batches), total=n_batches):
            imgs = np.array([self.preprocess(im) for im in batch])
            if ix == 0: _preds = self.ocr.predict(imgs)
            else:
                _preds = np.r_[_preds, self.ocr.predict(imgs)]

        if vocab is None: vocab = self.charList
        valid_ixs = [self.charList.index(i) for i in vocab] + [len(self.charList)]
        invalid_ixs = [i for i in range(len(self.charList)+1) if i not in valid_ixs]
        _preds[:,:,invalid_ixs] = 0

        if return_array: return _preds
        preds, confs = [], []
        for pred in _preds:
            prob = list(zip(pred.argmax(-1), pred.max(-1)))
            prob = [(self.int2char[c], conf) for c, conf in prob]
            tup = [(k, max(g, key=lambda x: x[1])[1]) for k, g in groupby(prob, key=lambda x: x[0])]
            if debug: print([t for t in tup if t[1]!='`'])
            if conf_agg == 'min':
                conf = min([p for p in tup if p != '`'], key=lambda x: x[1])[1]
            elif conf_agg == 'mean':
                conf = np.mean([p[1] for p in tup if p != '`'])
            else:
                raise NotImplementedError('Acceptable aggregate functions are `min` and `mean`')
            pred = self.decoder_chars(pred)
            preds.append(pred)
            confs.append(conf)
        return preds, confs

    def predict_beam(self, im, beam_size=25, vocab=None, debug=False, return_array=False):
        if vocab is None: vocab = self.charList
        valid_ixs = [self.charList.index(i) for i in vocab] + [len(self.charList)]
        invalid_ixs = [i for i in range(len(self.charList)+1) if i not in valid_ixs]
        img = self.preprocess(im)
        pred = self.ocr.predict(img[None])[0]
        pred[:,invalid_ixs] = 0
        if return_array: return pred
        beam_preds, beam_conf, lm_conf = self.order_preds_beam(pred, k=beam_size, alpha=0.8)
        # print('\n'.join([str(i) for i in list(zip(beam_preds, beam_conf, lm_conf))]))
        return beam_preds
        # return pred, conf
    def predict_beam_batch(self, imgs, vocab=None, debug=False, return_array = False):
        batch_size=4096
        N = len(imgs)
        n_batches = N//batch_size + 1
        def yield_batch(imgs):
            for i in range(n_batches):
                batch = imgs[i*batch_size:(i+1)*batch_size]
                yield batch
        batches = yield_batch(imgs)
        for ix, batch in tqdm.tqdm(enumerate(batches), total=n_batches):
            imgs = np.array([self.preprocess(im) for im in batch])
            if ix == 0: _preds = self.ocr.predict(imgs)
            else:
                _preds = np.r_[_preds, self.ocr.predict(imgs)]

        if vocab is None: vocab = self.charList
        valid_ixs = [self.charList.index(i) for i in vocab] + [len(self.charList)]
        invalid_ixs = [i for i in range(len(self.charList)+1) if i not in valid_ixs]
        _preds[:,:,invalid_ixs] = 0

        if return_array: return _preds
        preds, confs = [], []
        for pred in _preds:
            beam_preds, beam_conf, lm_conf = self.order_preds_beam(pred)[0]
            preds.append(beam_preds[0])
            confs.append(1)
        return preds, confs

    def cer(self, preds, labels):
        assert isinstance(preds, (list, tuple))
        assert isinstance(labels, (list, tuple))
        assert len(preds) == len(labels)
        c, d = [], []
        for p, l in zip(preds, labels):
            # c.append(editdistance.eval(p, l))
            # d.append(len(l))
            c.append(editdistance.eval(p, l) / len(l))
        # return np.sum(c)/np.sum(d)
        return round(np.mean(c), 4)

    def wer(self, preds, labels):
        assert isinstance(preds, (list, tuple))
        assert isinstance(labels, (list, tuple))
        assert len(preds) == len(labels)
        c = 0
        for p, l in zip(preds, labels):
            c += p.lower().strip() != l.lower().strip()
        return round(c/len(preds), 4)

    def evaluate(self, ims, labels, lower=False):
        ims = np.array([self.preprocess(im) for im in ims])
        preds = self.ocr.predict(ims, verbose=1)
        preds = [self.decoder_chars(pred) for pred in preds]
        if lower:
            preds = [p.lower() for p in preds]
            labels = [l.lower() for l in labels]
        return {'char-error-rate': self.cer(preds, labels),
                'word-error-rate': self.wer(preds, labels),
                'char-accuracy'  : 1 - self.cer(preds, labels),
                'word-accuracy'  : 1 - self.wer(preds, labels)}

    def preprocess_old(self, img, random_placement=False, noise=False):
        # img = top_down_margin(img)
        # if random.random() < 0.2:
        #     img = underline_image(img)
        img = img.mean(-1).astype(np.uint8) if len(img.shape) == 3 else img
        target = np.ones((40, 400))*255
        new_x = 40/img.shape[0]
        new_y = 400/img.shape[1]
        min_xy = min(new_x, new_y)
        new_x = int(img.shape[0]*min_xy)
        new_y = int(img.shape[1]*min_xy)
        if not all([new_x,new_y]): return np.zeros((40,400))
        img2 = cv2.resize(img, (new_y,new_x))
        if random_placement:
            try:
                h, w = img2.shape
                x, y = 0, 0
                if h == 40:
                    x = np.random.randint(400 - w)
                    target[:,x:x+new_y] = img2
                if w == 400:
                    y = np.random.randint(40 - h)
                    target[y:y+new_x,:] = img2
            except:
                target[:new_x,:new_y] = img2
        if not random_placement:
            target[:new_x,:new_y] = img2
        if noise:
            target = add_noise(target)
        target = (target > 150)*255
        return 1 - (target)/255

    def preprocess(self, img, random_placement=False, noise=False):
        img = img.mean(-1).astype(np.uint8) if len(img.shape) == 3 else img
        target = np.ones((40, 400))*255
        new_x = 40/img.shape[0]
        new_y = 400/img.shape[1]
        min_xy = min(new_x, new_y)
        new_x = int(img.shape[0]*min_xy)
        new_y = int(img.shape[1]*min_xy)
        if not all([new_x,new_y]): return np.zeros((40,400))
        img2 = cv2.resize(img, (new_y,new_x))
        target[:new_x,:new_y] = img2
        return 1 - (target)/255

def shrink(img, random_placement=False, noise=False):
    img = img.mean(-1).astype(np.uint8) if len(img.shape) == 3 else img
    target = np.ones((40, 400))*255
    new_x = 40/img.shape[0]
    new_y = 400/img.shape[1]
    min_xy = min(new_x, new_y)
    new_x = int(img.shape[0]*min_xy)
    new_y = int(img.shape[1]*min_xy)
    if not all([new_x,new_y]): return np.zeros((40,400))
    img2 = cv2.resize(img, (new_y,new_x))
    return img2
